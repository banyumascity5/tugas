import {Text, View, Button, TextInput, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      warnaText: 'blue',
      text: '',
      text1: '',
      fulltext : '',
    };
  }

  ubahwarna = () => {
    const warna = this.state.warnaText;
    if (warna === 'blue'){
      this.setState({warnaText: 'green'});
    } else if (warna === 'green') {
      this.setState({warnaText: 'red'});
    } else {
      this.setState({warnaText: 'blue'});
    }
  };

  fullname = () => {
    alert ("full name : " + this.state.text +" "+ this.state.text1)
  }

  render () {
    return (
      <View style ={style.component}> 
      <Text style={{color: this.state.warnaText}}> Halo React Native </Text>
      <TouchableOpacity style={style.btn} onPress={() => this.ubahwarna()}>
        <Text>Klik</Text>
      </TouchableOpacity>
      <TextInput
        style={{height: 40, borderColor: 'grey', borderWidth: 1, fontSize: 16}}
        onChangeText={text => this.setState({text : text})}
        placeholder= 'First Name'
        value={this.state.text}
      />
      <TextInput
        style={{height: 40, borderColor: 'grey', borderWidth: 1, fontSize: 16}}
        onChangeText={text1 => this.setState({text1 : text1})}
        placeholder= 'Last Name'
        value={this.state.text1}
      />
      <Button
      title='fullname'
      color='green'
      onPress={this.fullname}
      />
      
      </View>
    );
  }
}


export default App;

const style= StyleSheet.create({
  component: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'green',
    padding: 5,
  },
  input: {
    backgroundColor: 'green',
    margin: 2,
    width: '100%',
    paddingHorizontal

  }
  
})